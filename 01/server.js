var net = require('net')

var server = net.createServer(function(socket) {
    console.log('new socket connection')
    socket.on('data', function(data) {
        socket.write(data)
    })
})

server.listen(6666, function() {
    console.log('server started')
})