require('lookup-multicast-dns/global')
var register = require('register-multicast-dns')
var toPort = require('hash-to-port')
var topology = require('fully-connected-topology')
var jsonStream = require('duplex-json-stream')

function genAddress(nickname) {
    return `${nickname}.local:${toPort(nickname)}`
}

var nickname = process.argv[2] || 'any'
var peers = process.argv.slice(2).map(genAddress)

register(`${nickname}.local`)

console.log(peers)

var swarm = topology(peers[0], peers.slice(1))

var allSockets = new Set()
var seqByIds = {}

var from = Math.random()
var seq = 0

swarm.on('connection', function (socket, id) {
    console.log('new connection from', id)
    socket = jsonStream(socket)
    allSockets.add(socket)
    socket.on('data', function (data) {
        if (data.from == from || seqByIds[data.from] >= data.seq)
            return // is a known message
        seqByIds[data.from] = data.seq
        console.log(`${data.nickname}> ${data.message}`)
        for (let s of allSockets) {
            s.write(data)
        }
    })
})

process.stdin.on('data', function (data) {
    var message = data.toString().trim()
    if (message.length > 0) {
        for (let s of allSockets) {
            s.write({
                from,
                seq,
                nickname,
                message,
            })
        }
        seq++;
    }
})