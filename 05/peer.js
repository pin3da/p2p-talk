require('lookup-multicast-dns/global')
var register = require('register-multicast-dns')
var toPort = require('hash-to-port')
var topology = require('fully-connected-topology')

function genAddress(nickname) {
    return `${nickname}.local:${toPort(nickname)}`
}

var nickname = process.argv[2] || 'any'
var peers = process.argv.slice(2).map(genAddress)

register(`${nickname}.local`)

console.log(peers)

var swarm = topology(peers[0], peers.slice(1))

swarm.on('connection', function(socket, id) {
    console.log('new connection from', id)
})