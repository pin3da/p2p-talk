const net = require('net')

var socket = net.connect(6666, 'localhost')

process.stdin.on('data', function(data) {
    var nick = data.toString().trim()
    if (nick.length > 0)
        socket.write(nick);
});

socket.on('data', function(data) {
    console.log(data.toString().trim());
});