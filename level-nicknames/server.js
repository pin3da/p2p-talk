const levelup = require('levelup')
const leveldown = require('leveldown')
const net = require('net')

var db = levelup(leveldown('./mydb'))

function store(nickname, socket) {
    db.put(nickname, 'taken', function (err) {
        if (err) {
            console.error(err);
        } else {
            socket.write(`"${nickname}" assigned to you`)
            console.log(nickname, "assigned")
        }
    })
}

function assign(nickname, socket) {
    db.get(nickname, function (err, value) {
        if (err) { // Not found
            store(nickname, socket)
        } else {
            socket.write(`sorry, "${nickname}" is taken, try another nickname`)
        }
    })
}

var server = net.createServer(function (socket) {
    console.log('new connection')
    socket.on('data', function (nickname) {
        nickname = nickname.toString().trim()
        console.log('tried with nickname:', nickname)
        assign(nickname, socket)
    });
})

server.listen(6666)