# p2p workshop notes

My solutions and notes of [p2p workshop](https://mafintosh.github.io/p2p-workshop/build/01.html).

Problems 1 to 4 are identical to the original ones. Problems about p2p were re-ordered
for better understanding.

## Motivation

Problems with the current model:

- It is very expensive to "scale". Some people can not publish their website or app because they do not have servers.

- Information is owned by the companies, not by the authors

- What if the site is suddenly shut down ? (Egypt 2011 - 2012)

- The content may appear/disappear at *any* moment, even in the past (like in 1984).

- The content of a link may be changed at any moment.

- does not work offline.

Solutions:

- how to build systems that nobody can own ?

- how to build systems that can't be shut down ?

- how to build a web that scales ?

Pros:

- trust

- data distribution

- hosting

- anonymity

Cool projects using decentralized solutions:

- Bittorrent
- [SETI@home](https://en.wikipedia.org/wiki/SETI@home)
- [IPFS](https://ipfs.io/): A peer-to-peer hypermedia protocol to make the web faster, safer, and more open.
- [Dat Project](https://datproject.org/): Dat is a nonprofit-backed data sharing protocol for applications of the future.
- [Beaker Browser](https://beakerbrowser.com/): Beaker is an experimental browser for exploring and building the peer-to-peer Web.
- [peermaps](http://peermaps.github.io/)

## Problems description/hints

### Client/server architecture

- 01: Echo server and client.

> use the module `net` with the function `createServer`.

- 02: Minimal chat server.

> use a set to keep track of all connections and forward all the messages.

- 03: Chat with nicknames.

> read the nickname as program argument

> add the module `duplex-json-stream`

- 04: Add multicast DNS (work in groups from this point)

> add `require('lookup-multicast-dns/global')` on the client

> add `var register = require('register-multicast-dns'); register('this-is-a-test.local')` on the server

### P2P architecture

- 05: Fully connected topology

> add `hash-to-port` and generate address from nickname

> add `fully-connected-topology`

- 06: Fully connected p2p chat

> add the same logic from 02 here.

- 07: Gossip p2p chat

> create a network which is not fully-connected

> test it to see that not all the messages are delivered!

> add `idFrom` and `seq` (idFrom must be random)

> skip the known messages

### Distributed append only logs

- [James Halliday - What you can build with a log - BrazilJS 2015](https://www.youtube.com/watch?v=RPFjN1N148U)

### People to follow

- [@substack](https://twitter.com/substack)
- [@mafintosh](https://twitter.com/mafintosh)
- [@dat_project](https://twitter.com/dat_project)
- [@denormalize](https://twitter.com/denormalize)
- [pfrazee](https://twitter.com/pfrazee)

-----
by

Manuel Pineda [@gitlab](https://gitlab.com/pin3da)  [@github](https://github.com/pin3da)