var net = require('net')
var jsonStream = require('duplex-json-stream')

var clients = new Set()
var server = net.createServer(function (socket) {
    console.log('new socket connection')
    socket = jsonStream(socket)
    clients.add(socket)
    socket.on('data', function (data) {
        console.log(data)
        for (let client of clients) {
            if (client != socket) {
                client.write({
                    data: `${data.nickname}> ${data.data}`
                })
            }
        }
    })
})

server.listen(6666, function () {
    console.log('server started')
})