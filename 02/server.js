var net = require('net')


var clients = new Set()
var server = net.createServer(function (socket) {
    console.log('new socket connection')
    clients.add(socket)
    socket.on('data', function (data) {
        for (let client of clients) {
            if (client != socket) {
                client.write(data)
            }
        }
    })
})

server.listen(6666, function () {
    console.log('server started')
})